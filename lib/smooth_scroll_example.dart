import 'package:flutter/material.dart';
import 'package:smooth_scroll/random_color.dart';
import 'package:smooth_scroll_web/smooth_scroll_web.dart';

class SmoothScrollExample extends StatelessWidget {
  ScrollController controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SmoothScroll Example"),
      ),
      body: SmoothScrollWeb(
        child: _getChild(),
        controller: controller,
      ),
    );
  }

  Widget _getChild() {
    return Container(
      height: 1000,
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        controller: controller,
        children: [
          for (int i = 0; i < 100; i++)
            Container(
              height: 60,
              color: RandomColor.generate(),
            ),
        ],
      ),
    );
  }
}
