import 'dart:math';

import 'package:flutter/material.dart';

class RandomColor {
  static Color generate() {
    Random rnd = Random();
    return Color.fromRGBO(rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255), 1);
  }
}
